/*
fft_adc_serial.pde
guest openmusiclabs.com 7.7.14
example sketch for testing the fft library.
it takes in data on ADC0 (Analog0) and processes them
with the fft. the data is sent out over the serial
port at 115.2kb.
*/

#define LOG_OUT 1 // use the log output function
#define FFT_N 256 // set to 256 point fft

#define NTONES 3
#define NOISELED 7
#define UNLOCKEDLED 8
#define TONE1LED 4
#define TONE2LED 5
#define TONE3LED 6
int tnList[NTONES] = {8, 9, 10};
int tnLen[NTONES] = {50, 50, 10};
int tnErr = 1; // tnLen variability allowed
int tnCnt = 0; // tone counts
int tnInd = 0; // index to current tone listening for
int lastTn = 0; // last tone heard
int tnThresh = 115; // threshold to call it a received tone
int outCnt = 0;  // count for lighting LED to show correct

#include <FFT.h> // include the library

void setup() {
  Serial.begin(115200); // use the serial port
  pinMode(NOISELED, OUTPUT);
  pinMode(UNLOCKEDLED, OUTPUT);
  pinMode(TONE1LED, OUTPUT);
  pinMode(TONE2LED, OUTPUT);
  pinMode(TONE3LED, OUTPUT);
  TIMSK0 = 0; // turn off timer0 for lower jitter
  ADCSRA = 0xe5; // set the adc to free running mode
  ADMUX = 0x40; // use adc0
  DIDR0 = 0x01; // turn off the digital input for adc0
}

void loop() {
  while(1) { // reduces jitter
    //digitalWrite(7, HIGH);
    digitalWrite(NOISELED, HIGH);
    cli();  // UDRE interrupt slows this way down on arduino1.0
    for (int i = 0 ; i < 512 ; i += 2) { // save 256 samples
      while(!(ADCSRA & 0x10)); // wait for adc to be ready
      ADCSRA = 0xf5; // restart adc
      byte m = ADCL; // fetch adc data
      byte j = ADCH;
      int k = (j << 8) | m; // form into an int
      k -= 0x0200; // form into a signed int
      k <<= 6; // form into a 16b signed int
      fft_input[i] = k; // put real data into even bins
      fft_input[i+1] = 0; // set odd bins to 0
    }
    fft_window(); // window the data for better frequency response
    fft_reorder(); // reorder the data before doing the fft
    fft_run(); // process the data in the fft
    fft_mag_log(); // take the output of the fft
    sei();
    //Serial.println("start");
    int mxF = 0;
    int mxInd = 0;
    for (byte i = 0 ; i < FFT_N/2 ; i++) { 
      if(i > 1)
      {
        if(fft_log_out[i] > mxF)
        {
          mxF = fft_log_out[i];
          mxInd = i;
        }
      }
      //Serial.println(fft_log_out[i]); // send out the data
    }
    digitalWrite(TONE1LED, HIGH);
    digitalWrite(TONE2LED, HIGH);
    digitalWrite(TONE3LED, HIGH);
    if(mxF > tnThresh)
    {
      digitalWrite(NOISELED, LOW);
      Serial.print("Mag: ");
      Serial.print(mxF);
      Serial.print("\tmxInd: ");
      Serial.println(mxInd);
      // recognize sequence of tones
      Serial.print("lastTn = ");
      Serial.print(lastTn);
      Serial.print(", tnInd = ");
      Serial.print(tnInd);
      Serial.print(", tnCnt = ");
      Serial.print(tnCnt);
      Serial.print(", tnList[tnInd] = ");
      Serial.println(tnList[tnInd]);
      if(mxInd == tnList[tnInd]) // tone matches desired tone
      {
        tnCnt++; // increment tone counter
        if((tnInd == (NTONES - 1)) && (tnCnt > (tnLen[tnInd] - tnErr))) // on last tone
        { // this is a hack here. Need to go back through logic for all tones' count
          Serial.println("You did it!!!");
          //delay(3000); // probably doesn't work in this setup
          outCnt = 300;
          // reset to look for another
          tnInd = 0;
          tnCnt = 0;
        }
      } else 
      {
        if(tnInd == (NTONES - 1)) // on last tone
        {
          Serial.println("You did it!!!");
          //delay(3000); // probably doesn't work in this setup
          outCnt = 300;
          // reset to look for another
          tnInd = 0;
          tnCnt = 0;
        } else  // not on last tone
        {
          if(lastTn == tnList[tnInd]) // last tone was correct
          {
            Serial.println("Last tone correct");
            if(mxInd == tnList[++tnInd]) // current tone is next tone
            {
              Serial.println("Current tone is next tone");
              tnCnt = 0; // reset tone counter
            } else // reset
            {
              Serial.println("Current tone is not next tone");
              tnInd = 0;
              tnCnt = 0;
            }
          } else // last tone not correct
          {
            Serial.println("Last tone was not correct");
            tnInd = 0;
            tnCnt = 0;
          }
        }
      }
      /*
      // tone has changed so check if at right time
      if((mxInd != lastTn) && (lastTn == tnList[tnInd]))
      {
        if((tnCnt >= (tnLen[tnInd] - tnErr)) && (tnCnt <= (tnLen[tnInd] + tnErr)))
        { // right tone lasted right length
          if(tnInd == (NTONES - 1)) // we have succeeded
          {
            Serial.println("You did it!!!");
            delay(3000);
            // reset to look for another
            tnInd = 0;
            tnCnt = 0;
          } else
          {
            // move to the next tone
            tnInd++;
            tnCnt = 0;
          }
        } else // tone wrong length
        {
          tnInd = 0;
          tnCnt = 0;
          Serial.println("Wrong length, resetting");
        }
      }
      */
 //     if(mxInd == 2)
 //       digitalWrite(3, LOW);
      if(mxInd == tnList[0])
      {
        digitalWrite(TONE1LED, LOW);
      }
      if(mxInd == tnList[1])
      {
        digitalWrite(TONE2LED, LOW);
      }
      if(mxInd == tnList[2])
      {
        digitalWrite(TONE3LED, LOW);
      }
      lastTn = mxInd; // update last tone heard
    }
    if(outCnt > 0) // light LED
    {
      digitalWrite(UNLOCKEDLED, LOW);
      outCnt--;
    } else
    {
      digitalWrite(UNLOCKEDLED, HIGH);
    }
  }
}
